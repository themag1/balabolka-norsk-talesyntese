@echo off
echo:  
echo Dette er et powershell-skript som lar deg bruke alle nedlasta spraak i Windows i Balabolka..
echo:   
echo M├Ñ kjores som administrator!
pause
echo :
echo -------------- Skript start --------------
:: variabelen %~dp0 er mappe som skriptet startes fra
powershell.exe -executionpolicy bypass -file "%~dp0balabolka-skript.ps1"
echo -------------- Skript slutt --------------
echo:  
echo:  
echo Hvis du ikke fikk noen feilmeldinger er alt mest sannsynlig i orden. :-)
echo Start Balabolka paa nytt og sjekk om nye spraak er der!
echo:    
echo:  
pause

