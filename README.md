# Balabolka Norsk Talesyntese

Balabolka er et program som kan konvertere PDFer til mp3 (og tekst). Det lar deg bruke stemmene som ligger i Windows 10 (og 11?) og høres derfor ganske bra ut. 

Last det ned her: http://balabolka.site/balabolka.htm

# Hvordan få inn norsk og andre språk i Balabolka?
Basert på https://stephenmonro.wordpress.com/2020/06/17/text-to-speech-wav/

**Først:** Legg til ønskede språk i Windows (10) under: *Innstillinger -> Tid og språk -> Tale*

Hvis du aldri har kjørt et powershell-skript før (eller ikke vet hva det er), kan du gjøre følgende:
1. Last ned balabolka-skript.ps1: https://gitlab.com/themag1/balabolka-norsk-talesyntese/-/blob/main/balabolka-skript.ps1
2. Last ned balabolka-skript.bat: https://gitlab.com/themag1/balabolka-norsk-talesyntese/-/blob/main/balabolka-skript.bat
3. Sørg for at de to ovenstående filene ligger i samme mappe
4. Dobbeltklikk på balabolka-skript.bat-filen du lastet ned 
5. Følg "instruksjonene" på skjermen og kryss fingrene for at alt går bra :)

Hvis du vet hva du driver med kan du kjøre følgende skript, kjøres kun én gang: 
``` powershell
$sourcePath = 'HKLM:\software\Microsoft\Speech_OneCore\Voices\Tokens' #Where the OneCore voices live
$destinationPath = 'HKLM:\SOFTWARE\Microsoft\Speech\Voices\Tokens' #For 64-bit apps
$destinationPath2 = 'HKLM:\SOFTWARE\WOW6432Node\Microsoft\SPEECH\Voices\Tokens' #For 32-bit apps
cd $destinationPath
$listVoices = Get-ChildItem $sourcePath
foreach($voice in $listVoices)
{
$source = $voice.PSPath #Get the path of this voices key
copy -Path $source -Destination $destinationPath -Recurse
copy -Path $source -Destination $destinationPath2 -Recurse
}
```

# En slags workflow for konvertering av PDF til mp3
1. Åpne Tools -> Batch convert i balabolka - til TEKST
2. Åpne i en litt avansert teksteditor (Word, Notepad++) eller lignende
3. Fjern bunntekster og annet irrelevant vrøvl
4. Her er det også lurt å 
5. Search and repace \\n\\r" med mellomrom for å fjerne alle nye linjer (blir pauser i balabolka) - i Word bruk \^p og erstatt med mellomrom
6. Lagre tekst-filen (det er bra å bruke .txt-formatet)
7. Åpne Tools -> Batch convert igjen
8. Legg inn de  redigerte-txtfilene og konverter de til mp3

# Avspilling
Jeg kopierer de til Android og spiller av med appen Voice Audiobook Player (https://play.google.com/store/apps/details?id=de.ph1b.audiobook) - Fungerer best om hver MP3 ligger i egen mappe. 

Synkroniserer de til Android ved hjelp av syncthing, men hva som helst fungerer jo.
